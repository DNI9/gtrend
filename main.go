package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"unicode"

	"github.com/PuerkitoBio/goquery"
)

var baseUrl string = "https://github.com/trending/"
var timeSpans = []string{"daily", "weekly", "monthly"}
var fileName = "trending.csv"

type repo struct {
	name        string
	owner       string
	description string
	url         string
	stars       string
	lang        string
}

func main() {
	timeSpanPtr := flag.String("timespan", "daily", "sets time span, available options: daily, weekly, monthly")
	langPtr := flag.String("lang", "", "specifies language to search, e.g. JavaScript, Go, etc")
	flag.Parse()

	if *timeSpanPtr != "daily" && !contains(timeSpans, *timeSpanPtr) {
		log.Fatalln("please use right values for timespan, available options: daily, weekly, monthly")
	}

	fileName = fmt.Sprintf("trending_%s_%s.csv", *timeSpanPtr, *langPtr)

	res, err := http.Get(baseUrl + *langPtr + "?since=" + *timeSpanPtr)
	handleError(err)
	defer res.Body.Close()
	checkStatus(res)

	extractRepos(res)
}

func extractRepos(res *http.Response) {
	c := make(chan repo)
	writeC := make(chan string)

	var repos []repo

	doc, err := goquery.NewDocumentFromReader(res.Body)
	handleError(err)

	article := doc.Find("article.Box-row")
	article.Each(func(i int, s *goquery.Selection) {
		go extractSingleRepo(s, c)
	})

	for i := 0; i < article.Length(); i++ {
		repo := <-c
		repos = append(repos, repo)
	}

	go exportToCSV(repos, writeC)
	fmt.Println(<-writeC)
}

func extractSingleRepo(s *goquery.Selection, c chan repo) {
	url, _ := s.Find("h1 a").Attr("href")
	otherInfo := strings.Fields(strings.TrimSpace(s.Find("div.f6").Text()))

	owner := strings.Split(url, "/")[1]
	repoName := strings.Split(url, "/")[2]
	desc := strings.TrimSpace(s.Find("p").Text())
	lang := otherInfo[0]
	stars := otherInfo[1]

	if isInt(lang) {
		lang = ""
	}

	c <- repo{
		name:        repoName,
		owner:       owner,
		description: desc,
		lang:        lang,
		stars:       stars,
		url:         "https://github.com/" + owner + "/" + repoName,
	}
}

func exportToCSV(repos []repo, c chan string) {
	file, err := os.Create(fileName)
	handleError(err)

	w := csv.NewWriter(file)
	defer w.Flush()

	headers := []string{"Name", "Owner", "Description", "Link", "Stars", "Language"}
	wErr := w.Write(headers)
	handleError(wErr)

	for _, repo := range repos {
		repoSlice := []string{repo.name, repo.owner, repo.description, repo.url, repo.stars, repo.lang}
		err := w.Write(repoSlice)
		handleError(err)
	}
	c <- "Saved trending list in " + fileName
}

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func checkStatus(res *http.Response) {
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}
}

// check if given string is integer or not
func isInt(s string) bool {
	for _, c := range s {
		if c == ',' {
			continue
		}
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
