# gtrend
Command line tool to save github trending repos to csv files.

## Building
```bash
# Install dependencies
go mod tidy

# then build
go build
```

## Usage

```bash
# by default it gets daily trending lists
./gtrend

# also you can specify time span and which language to search
./gtrend -timespan weekly -lang go

# Get help
./gtrend -h

```

